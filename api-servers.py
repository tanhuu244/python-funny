#!/usr/bin/python2.7
from subprocess import Popen, PIPE
import json
import numpy
import pandas
from email.MIMEText import MIMEText
from email.MIMEMultipart import MIMEMultipart
from smtplib import SMTP
import datetime
from datetime import datetime

dic_server = {}
array = [] 
row_labels = []
column_labels = ['Totalram(MB)', 'Ramuse(MB)', 'Totalspace(GB)', 'Spaceuse(GB)', 'Vcorelimit', 'Vcoreuse', 'State']

def server_name(name, vcore_limit):
    cmd = Popen('php /root/listserver.php', shell=True, stdout=PIPE, stderr=PIPE)
    listname = []
    _stdout, _stderr = cmd.communicate()
    dstdout = json.loads(_stdout)
    dstdout.keys()
    dstdout_server = dstdout['servers']
    row_labels.append(name)
    for k, v in dstdout_server.items():
        if str(v['server_name']) == name:
            listname.append(int(v['total_ram']))
            try:
                listname.append(int(v['alloc_ram']))
            except KeyError:
                v['alloc_ram']= 0                
                listname.append(0)
            listname.append(int(v['total_space']))
            listname.append(int(v['total_space'])-int(v['space']))
            listname.append(int(vcore_limit))
            listname.append(int(v['vcores']))
            array.append(listname)
#            print(name) 
#            print(listname)
#            print('________________________________________________')
            if int(v['alloc_ram']) >= int(v['total_ram']) or (int(v['total_space'])-int(v['space'])) >= (v['total_space']) or int(v['vcores']) >= vcore_limit:
       #        print('{0} is {1}'.format(v['server_name'], 'block'))
                listname.append('Warning')
 
       #     if (int(v['total_space'])-int(v['space'])) >= (v['total_space']):
       #        print('{0} is {1}'.format(v['server_name'], 'block'))
       #         listname.append('Warning')
       #      if int(v['vcores']) >= vcore_limit:
       #        print('{0} is {1}'.format(v['server_name'], 'block'))
                
       #     print(int(v['total_ram']))
       #     print(int(v['ram']))
       #     print(int(v['total_space']))
       #     print(int(v['space']))
       #     print(int(v['vcores']))
#    print(listname)


def sendmail(df):
    smtp = SMTP()
    strMessageBody = df + '<br><br>'
    strMessageBody += '<i>Thanks & Best Regards,</i>' + '<br>'
    strMessageBody += '<b></b>'
    smtp.connect('', 587)
    smtp.login('', '')
    from_addr = ''
    to_addr = ''
    msg = MIMEMultipart()
    msg.attach(MIMEText(strMessageBody, 'html', "utf-8"))
    msg['Subject'] = ''
    msg['To'] = to_addr
    msg['From'] = ''
    subj = ''
    date = datetime.now().strftime("%d-%m-%y")
   # msg = 'From: %s\nTo: %s\nSubject: %s\nDate: %s\n\n%s' % (from_addr, to_addr.split(','), subj, date, df2)
    smtp.sendmail(from_addr, to_addr.split(','), msg.as_string())
    smtp.quit()



server_name(name='localhost', vcore_limit=20)
server_name(name='node01', vcore_limit=60)
server_name(name='node02', vcore_limit=60)
server_name(name='node03', vcore_limit=60)
#server_name(name='node04', vcore_limit=20)
#server_name(name='node05', vcore_limit=60)
server_name(name='node06', vcore_limit=60)
server_name(name='node07', vcore_limit=80)
#server_name(name='node08', vcore_limit=60)
server_name(name='node09', vcore_limit=60)
server_name(name='node10', vcore_limit=60)
server_name(name='node11', vcore_limit=60)
server_name(name='node12', vcore_limit=80)
server_name(name='node14', vcore_limit=80)
server_name(name='node15', vcore_limit=60)
server_name(name='node16', vcore_limit=80)
server_name(name='node17', vcore_limit=60)
server_name(name='node18', vcore_limit=60)
#array = numpy.array(array)
#print(type(array))
#print(array)
df = pandas.DataFrame(array, columns=column_labels, index=row_labels).to_html()
#print(df)
#print(array)
#print(column_labels)
#print(row_labels)
#df2 = df.to_html()
#sendmail(df)
