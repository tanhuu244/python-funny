from subprocess import Popen, PIPE
from pySMART import Device
# pySMART module imports
device = {'sda':1}
for k, v in device.items():
    print(k, v)
    cmd = Popen('smartctl -d sat+megaraid,{1} -a /dev/{0}'.format(k, v), shell=True, stdout=PIPE, stderr=PIPE)
    _stdout, _stderr = cmd.communicate()
    parse_self_tests = False
    parse_ascq = False
    for line in _stdout.split('\n'):
        if 'Device Model' in line:
            model = line.split(':')[1].lstrip().rstrip()
            print(model)
        if 'SMART overall-health self-assessment' in line:
            result = line.split(':')[1].lstrip().rstrip()
            print(result)
        if '0x0' in line and '_' in line:
            line= ' '.join(line.split()).split(' ')
            getvalue = line[8]
            if getvalue == 'In_the_past' and getvalue == 'FAIL' and getvalue == 'FAILING_NOW':
                print('Fail')
        if 'Error: UNC at LBA' in line:
            pointerr = line.split(':')[1].lstrip().rstrip()
            print(pointer)

