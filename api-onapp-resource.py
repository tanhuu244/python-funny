#!/usr/bin/python2.7
import requests
import json
import numpy
import pandas
from email.MIMEText import MIMEText
from email.MIMEMultipart import MIMEMultipart
from smtplib import SMTP
import datetime
from datetime import datetime
from requests.auth import HTTPBasicAuth

dic_group = {}
row_labels = []
column_labels = ['Cpu_cores', 'Total_cpus', 'Use_cpuresources(%)', 'Totalmemory(MB)', 'Mem_allocated_vms(MB)', 'Mem_allocated_runingvms(MB)', 'Location', 'State']
row_labels = []
array = []
def getlist_hypervisor_zones():
    reqs_hypervisor_zones = requests.get('url_api', auth=('tannh', 'password'))
    data_reqs_hypervisor_zones = json.loads(reqs_hypervisor_zones.text)
    for i in data_reqs_hypervisor_zones:
        dic_group.update({i['hypervisor_group']['id']:i['hypervisor_group']['label']})
    return dic_group    

def getlist_hypervisor_vdc():
    listname = []
    reqs_hypervisor_vdc = requests.get('url_api', auth=('tannh', 'password'))
    data_hypervisor_vdc = json.loads(reqs_hypervisor_vdc.text)
    for i in data_hypervisor_vdc:
        if i['hypervisor']['hypervisor_group_id'] == 3:
            row_labels.append(i['hypervisor']['label'])
            listname.append(i['hypervisor']['cpu_cores'])
            listname.append(i['hypervisor']['total_cpus'])
            listname.append(i['hypervisor']['used_cpu_resources'])
            listname.append(i['hypervisor']['total_memory'])
            listname.append(i['hypervisor']['total_memory_allocated_by_vms'])
            listname.append(i['hypervisor']['memory_allocated_by_running_vms'])
            for k, v in dic_group.items():
                if k == i['hypervisor']['hypervisor_group_id']:
                    listname.append(v)
            if i['hypervisor']['total_memory_allocated_by_vms'] > i['hypervisor']['total_memory']:
                listname.append('Warning')
            array_list(listname)
            listname = []
    #getlist_hypervisor_singapore()       

def getlist_hypervisor_singapore():
    listname = []
    reqs_hypervisor_singapore = requests.get('url_api', auth=('tannh', 'password'))
    data_hypervisor_singapore = json.loads(reqs_hypervisor_singapore.text)
    for i in data_hypervisor_singapore:
        row_labels.append(i['hypervisor']['label'])
        listname.append(i['hypervisor']['cpu_cores'])
        listname.append(i['hypervisor']['total_cpus'])
        listname.append(i['hypervisor']['used_cpu_resources'])
        listname.append(i['hypervisor']['total_memory'])
        listname.append(i['hypervisor']['total_memory_allocated_by_vms'])
        listname.append(i['hypervisor']['memory_allocated_by_running_vms'])
        listname.append('Singapore')
        if i['hypervisor']['total_memory_allocated_by_vms'] > i['hypervisor']['total_memory']:
                listname.append('Warning')
        array_list(listname)
        listname = []

def getlist_hypervisor_binhduong():
    listname = []
    reqs_hypervisor_binhduong = requests.get('url_api', auth=('tannh', 'password'))
    data_hypervisor_binhduong = json.loads(reqs_hypervisor_binhduong.text)
    for i in data_hypervisor_binhduong:
        row_labels.append(i['hypervisor']['label'])
        listname.append(i['hypervisor']['cpu_cores'])
        listname.append(i['hypervisor']['total_cpus'])
        listname.append(i['hypervisor']['used_cpu_resources'])
        listname.append(i['hypervisor']['total_memory'])
        listname.append(i['hypervisor']['total_memory_allocated_by_vms'])
        listname.append(i['hypervisor']['memory_allocated_by_running_vms'])
        listname.append('VietNam(BinhDuong)')
        if i['hypervisor']['total_memory_allocated_by_vms'] > i['hypervisor']['total_memory']:
                listname.append('Warning')
        array_list(listname)
        listname = []

def getlist_hypervisor_hanoi():
    listname = []
    reqs_hypervisor_hanoi = requests.get('arl_api', auth=('tannh', 'password'))
    data_hypervisor_hanoi = json.loads(reqs_hypervisor_hanoi.text)
    for i in data_hypervisor_hanoi:
        row_labels.append(i['hypervisor']['label'])
        listname.append(i['hypervisor']['cpu_cores'])
        listname.append(i['hypervisor']['total_cpus'])
        listname.append(i['hypervisor']['used_cpu_resources'])
        listname.append(i['hypervisor']['total_memory'])
        listname.append(i['hypervisor']['total_memory_allocated_by_vms'])
        listname.append(i['hypervisor']['memory_allocated_by_running_vms'])
        listname.append('VietNam(HaNoi)')
        if i['hypervisor']['total_memory_allocated_by_vms'] > i['hypervisor']['total_memory']:
                listname.append('Warning')
        array_list(listname)
        listname = []

        
def array_list(listname):
    array.append(listname)

def sendmail(df):
    smtp = SMTP()
    strMessageBody = df + '<br><br>'
    strMessageBody += '<i>Thanks & Best Regards,</i>' + '<br>'
    strMessageBody += '<b></b>'
    smtp.connect('', 587)
    smtp.login('', '')
    from_addr = ''
    to_addr = ''
    msg = MIMEMultipart()
    msg.attach(MIMEText(strMessageBody, 'html', "utf-8"))
    msg['Subject'] = '[Report summary][ONAPP RESOURCE]'
    msg['To'] = to_addr
    msg['From'] = 'Report summary'
    subj = ''
    date = datetime.now().strftime("%d-%m-%y")
    smtp.sendmail(from_addr, to_addr.split(','), msg.as_string())
    smtp.quit()

getlist_hypervisor_zones()
getlist_hypervisor_vdc()
getlist_hypervisor_singapore()
getlist_hypervisor_binhduong()
getlist_hypervisor_hanoi()

df = pandas.DataFrame(array, columns=column_labels, index=row_labels).to_html()

sendmail(df)
